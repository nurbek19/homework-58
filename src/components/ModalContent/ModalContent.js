import React from 'react';
import Wrapper from '../../hoc/Wrapper';

const ModalContent = props => {
    return (
        <Wrapper>

            <h1>{props.title}</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa cumque dolorum error ex harum minima numquam obcaecati officia, officiis optio repudiandae saepe sapiente sed, tenetur vero voluptatem voluptates? Atque aut delectus dolor dolorem, eos error itaque laboriosam maxime neque quae, quo reiciendis repellat reprehenderit repudiandae rerum sequi sint tempore vero!</p>
        </Wrapper>
    )
};

export default ModalContent;
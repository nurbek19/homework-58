import React, {Component} from 'react';
import Modal from './components/UI/Modal/Modal';
import Wrapper from './hoc/Wrapper';
import Button from './components/UI/Button/Button';
import Alert from './components/UI/Alert/Alert';
import './App.css';

class App extends Component {
    state = {
        purchasing: false
    };

    purchaseHandler = () => {
        this.setState({purchasing: true})
    };

    purchaseCancelHandler = () => {
        this.setState({purchasing: false})
    };

    purchaseContinueHandler = () => {
        alert('You continued');
    };

    dismissHandler = (event) => {
        event.target.parentNode.className += ' hide';
    };

    render() {
        const BUTTON_TYPES = [
            {type: 'Success', label: 'Continue', clicked: this.purchaseContinueHandler},
            {type: 'Danger', label: 'Close', clicked: this.purchaseCancelHandler}
        ];

        const modalBtns = BUTTON_TYPES.map((item, index) => {
            return <Button key={index} btnType={item.type} click={item.clicked}>{item.label}</Button>;
        });

        return (
            <div className="App">
                <Wrapper>
                    <Alert
                        type="danger"
                        dismiss={this.dismissHandler}
                    >This is a danger type alert</Alert>

                    <Alert
                        type="warning"
                        dismiss={this.dismissHandler}
                    >This is a danger type alert</Alert>

                    <Alert
                        type="success"
                        dismiss={this.dismissHandler}
                    >This is a danger type alert</Alert>

                    <button onClick={this.purchaseHandler} style={{marginTop: '20px'}}>Show Modal</button>

                    <Modal
                        title="Lorem ipsum dolor"
                        show={this.state.purchasing}
                        closed={this.purchaseCancelHandler}
                    >
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, neque!</p>
                        {modalBtns}
                    </Modal>
                </Wrapper>
            </div>
        )
    }
}

export default App;

import React from 'react';
import './Alert.css';

const Alert = props => {
    let dismiss;
    let btnStyle = {
        display: 'block'
    };

    if(props.dismiss === undefined) {
        btnStyle.display = 'none';
    } else {
        dismiss = props.dismiss;
    }

    return (
        <div
            className = {['Alert', props.type].join(' ')}
        >
            <span className="close-alert" style={btnStyle} onClick={dismiss}>x</span>
            {props.children}
        </div>
    )
};

export default Alert;